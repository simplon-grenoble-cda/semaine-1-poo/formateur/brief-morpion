# BRIEF : Jeu de Morpion

## Mise en situation 

Notre société souhaite développer un jeu de morpion, pour ses besoins internes (distraire ses employés :) ).
Le chef de projet a carte blanche pour le format et les feature du jeu. En revanche, il a un temps limité de ressources
humaines à y allouer. Le jeu doit être livré pour vendredi après midi, et les développeurs ne peuvent y consacrer qu'une
après midi par jour (les matinées sont déjà allouées à un autre projet).

Le chef de projet n'ayant pas l'expertise technique nécessaire pour établir un cahier des charges qui respecte le temps 
alloué, vous devez le mettre en place avec lui.

Objectifs : 

- Rédiger un cahier des charges qui défini explicitement le cadre de l'application
- Proposer plusieurs incrémentations du produit, afin d'avoir au minimum une version exploitable simple, 
  et potentiellement des versions avec plus de fonctionnalités si le temps le permet
- Développer le produit tel que décrit dans le cahier des charges
- Faire une livraison au chef de projet à la fin du temps alloué, et un recette du livrable avec lui

## Compétences visées par ce brief

Compétences conception : 

- Comprendre les besoins d'un client pour rédiger le cahier des charges d'un produit
- Définir le cadre d'une application, déceler les points qui demandent de l'éclaircicement

Compétences techniques : 

- Écrire un algorithme
- Coder dans un langage objet
- Sécuriser son code en adoptant un syle défensif
- Auto documenter son code au moyen du nommage
- Utilise un IDE
- Utiliser un outil collaboratif de partage de fichiers
